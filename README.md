# PAIP-go-haskell

In this project, we are implementing the case studies from the book "Paradigms of Artificial Intelligence Programming" by Peter Norwig using Go and Haskell.

## About

The book is majorly about three topics: the field of artificial intelligence, the skill of computer programming and the programming language Common Lisp. <br/>
<br/>
Lisp is one of the oldest programming languages. It supports assignment-oriented, state-oriented, object-oriented and also functional styles of programming. The case studies in the book are implemented using Common Lisp. We are implementing the same using Go and Haskell. 

## Table of Contents


1. General Problem Solver(GPS): Implementation using STRIPS approach
2. ELIZA: A program that mimics human dialogue
3. Building software Tools
4. STUDENT: A program that solves high school level algebra problems
5. Symbolic Mathematics: A simplification problem
6. Symbolic Mathematics with Canonical forms
7. Expert Systems
8. Search and the Game of Othello
9. Line-Diagram Labelling by Constraint Satisfaction
10. Introduction to Natural Language
11. Unification Grammars
12. A Grammar of English 


## Authors

Women Engineers Program - Cohort 3<br/>

1. Aarthi Shree Senthil Kumar
2. Aniva Maria Dsouza
3. Boddu Ramadevi
4. Induja Shankar
5. Khushi Sinha
6. Muskan Vaswan
7. Saachi Kaup
8. Sanya Bansal
9. Shalini C E
10. Shreya Mittal
11. Sristi

