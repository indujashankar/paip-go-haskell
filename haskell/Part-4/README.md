# Part 4

<description>

## List of Case Studies

- Case Study 6. Symbolic Mathematics with Canonical Form
- Case Study 7. Expert Systems
- Case Study 8. Line-Diagram Labeling by Constraint Satisfaction
- Case Study 9. Search and the Game of Othello
- Case Study 10. Introduction to Natural Language
- Case Study 11. Unification Grammars
- Case Study 12. A Grammar of English