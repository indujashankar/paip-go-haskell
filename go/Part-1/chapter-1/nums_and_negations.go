package main

// Numbers and Negations
//
// Problem: Given a list of elements, return a list consisting of
// all the numbers in the original list and the negation of those numbers.
//
// For example, given the list (testing 1 2 3 test), return (1 -1 2 -2 3 -3)
// In LISP, this problem can be solved using mappend. We shall write the same
// the style of Golang.
//
// LISP Code:
//
//     (defun numbers-and-negations (input)
//       "Given a list, return only the numbers and their negations."
//       (mappend #'number-and-negation input))
//
//     (defun number-and-negation (x)
//       "If X is a number, return a list of X and -X."
//       (if (numberp x)
//           (list x (- x))
//           nil))
//

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

// Implementation of numberp in Golang
func isInteger(s string) bool {
	if s[0] == '-' || s[0] == '+' {
		s = s[1:]
	}

	for _, c := range s {
		if c < '0' || c > '9' {
			return false
		}
	}
	return true
}

func numberAndNegation(x string) []int {
	if isInteger(x) {
		num, _ := strconv.Atoi(x)
		return []int{num, -num}
	}
	return []int{}
}

func numbersAndNegations(input []string) []int {
	var result []int
	for _, x := range input {
		result = append(result, numberAndNegation(x)...)
	}
	return result
}

func main() {
	args := os.Args[1:]
	if len(args) < 1 {
		fmt.Println(errors.New("No input provided"))
		os.Exit(1)
	} else {
		fmt.Println(numbersAndNegations(args))
	}
}
