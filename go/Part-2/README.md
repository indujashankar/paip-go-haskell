# Part 2

<description>

## List of Case Studies

- Case Study 1. GPS: General Problem Solver
- Case Study 2. ELIZA: Dialog with a Machine
- Case Study 3. Building Software Tools
- Case Study 4. STUDENT: Solving Algebra Word Problems
- Case Study 5. Symbolic Mathematics: A Simplification Program